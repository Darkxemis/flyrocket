package tk.makigas.chase.actor;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

import tk.makigas.chase.AlienChase;

public class MeteoritoActor extends Actor{
	/** Textura usada por el meteorito. */
	private TextureRegion meteorito;
	
	public Rectangle bb;

	public MeteoritoActor() {
		meteorito = new TextureRegion(AlienChase.MANAGER.get("meteorito.gif",
				Texture.class), 43, 29);//43 29
		setSize(meteorito.getRegionWidth(), meteorito.getRegionHeight());
		bb = new Rectangle(getX(), getY(), getWidth(), getHeight());
	}
	
	@Override
	public void act(float delta) {
		translate(-600 * delta, 0);
		bb.x = getX();
		bb.y = getY();
		bb.width = getWidth();
		bb.height = getHeight();
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		/*
		batch.draw(meteorito, getX(), getY(), getOriginX(), getOriginY(),
				getWidth(), getHeight(), getScaleX(), getScaleY(),
				getRotation());
				*/
		batch.draw(meteorito, getX(), getY(), getOriginX(), getOriginY(),
				getWidth(), getHeight(), getScaleX(), getScaleY(),
				getRotation());
	}
	
	
}
