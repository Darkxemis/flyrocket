package tk.makigas.chase.actor;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

import tk.makigas.chase.AlienChase;

public class RepairActor extends Actor{
	/** Textura usada por el repair. */
	private TextureRegion repair;
	
	public Rectangle bb;

	public RepairActor() {
		repair = new TextureRegion(AlienChase.MANAGER.get("repair.png",
				Texture.class), 43, 29);//43 29
		setSize(repair.getRegionWidth(), repair.getRegionHeight());
		bb = new Rectangle(getX(), getY(), getWidth(), getHeight());
	}
	
	@Override
	public void act(float delta) {
		translate(-400 * delta, 0);
		bb.x = getX();
		bb.y = getY();
		bb.width = getWidth();
		bb.height = getHeight();
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		batch.draw(repair, getX(), getY(), getOriginX(), getOriginY(),
				getWidth(), getHeight(), getScaleX(), getScaleY(),
				getRotation());
	}
}
