package tk.makigas.chase;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class Creditos extends AbstractScreen{
	private BitmapFont font;
	private Stage stage;

	
	public Creditos(AlienChase game) {
		super(game);
	}
	
	@Override
	public void render(float delta) {
		
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		game.SB.begin();
		game.SB.draw(creditos, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		 
		font.draw(game.SB, "<---------------------------------------------------->", 200, 360);
		font.draw(game.SB, "<-----------------FLYROCKET 1.0---------------->", 200, 340);
		font.draw(game.SB, "<---------------------------------------------------->", 200, 320);
        
		font.draw(game.SB, "M�sica Dise�os e imagenes de fondo", 200, 260);
		font.draw(game.SB, "http://opengameart.org/", 200, 240);
		font.draw(game.SB, "Pulsa escape para salir", 200, 200);
		font.draw(game.SB, "CREATE BY JOSEMI", 250, 20);
        
        
		game.SB.end();
		
		if(Gdx.input.isKeyPressed(Keys.ESCAPE)){
			game.setScreen(game.MAIN);
		}
	}
	
	private Texture creditos;
	
	
	@Override
	public void show() {
		stage = new Stage(640, 360, true,game.SB);//640 360
		creditos = AlienChase.MANAGER.get("creditos.png", Texture.class);
		//btnCreditos = AlienChase.MANAGER.get("creditos_boton.png", Texture.class);
		font = new BitmapFont();
		
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void draw(SpriteBatch batch) {
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
}
