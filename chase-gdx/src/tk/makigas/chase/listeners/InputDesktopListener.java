package tk.makigas.chase.listeners;

import java.util.List;

import tk.makigas.chase.AlienChase;
import tk.makigas.chase.actor.BulletActor;
import tk.makigas.chase.actor.NaveActor;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.TimeUtils;

public class InputDesktopListener extends InputListener {
	
	private NaveActor nave;
	
	private Stage stage;
	
	private List<BulletActor> bullets;
	
	private float timer = 1;
	
	public InputDesktopListener(NaveActor nave, Stage stage, List<BulletActor> bullets) {
		this.nave = nave;
		this.stage = stage;
		this.bullets = bullets;
	}
	
	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		switch(keycode) {
		case Input.Keys.UP:
			nave.velocidad.y = 550;
			return true;
		case Input.Keys.DOWN:
			nave.velocidad.y = -550;
			return true;
		case Input.Keys.LEFT:
			nave.velocidad.x = -550;
			return true;
		case Input.Keys.RIGHT:
			nave.velocidad.x = 550;
			return true;
		default:
			return false;
		}
	}

	@Override
	public boolean keyUp(InputEvent event, int keycode) {
		switch(keycode) {
		case Input.Keys.UP:
		case Input.Keys.DOWN:
			nave.velocidad.y = 0;
			return true;
		case Input.Keys.LEFT:
		case Input.Keys.RIGHT:
			nave.velocidad.x = 0;
			return true;
		default:
			return false;
		}
	}

	@Override
	public boolean keyTyped(InputEvent event, char character) {
		if(character != ' ')
			return false;		
		BulletActor bullet = new BulletActor();
		float x = nave.getX() + nave.getWidth() + 2;//2
		float y = nave.getY()-10 + nave.getHeight() / 2; //-10
		bullet.setPosition(x, y);
		if (TimeUtils.nanoTime() - timer > 300000000){
			stage.addActor(bullet);
			bullets.add(bullet);
			timer = TimeUtils.nanoTime();;
			AlienChase.MANAGER.get("shoot.ogg", Sound.class).play();
		}
		return true;
	}
}
