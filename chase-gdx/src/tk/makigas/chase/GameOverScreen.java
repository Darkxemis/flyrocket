package tk.makigas.chase;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import tk.makigas.chase.actor.PuntuacionActor;

public class GameOverScreen extends AbstractScreen {
	private BitmapFont font;
	public static int puntuacion_maxima;
	public GameOverScreen(AlienChase game) {
		super(game);
	}

	@Override
	public void render(float delta) {
		
        
		game.SB.begin();
		game.SB.draw(gameover, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		font.draw(game.SB, "PUNTOS MAXIMO OPTENIDOS: " + puntuacion_maxima, 20, 40);//getX()
		AlienChase.MANAGER.get("musicajuego.ogg", Sound.class).stop();
		AlienChase.MANAGER.get("musicagameover.ogg", Sound.class).play();
		game.SB.end();
		
		if(Gdx.input.isTouched()) {
			game.setScreen(game.MAIN);
			AlienChase.MANAGER.get("musicagameover.ogg", Sound.class).stop();
		}
	}
	
	private Texture gameover;
	
	public static void setPuntuacionMaxima(int puntuacion){
		puntuacion_maxima = puntuacion;
	}
	
	@Override
	public void show() {
		gameover = AlienChase.MANAGER.get("gameover.png", Texture.class);
		font = new BitmapFont();
	}
	
	@Override
	public void draw(SpriteBatch batch) {
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
