package tk.makigas.chase;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class AbstractScreen implements Screen {
	
	protected AlienChase game;

	public AbstractScreen(AlienChase game) {
		this.game = game;
	}
	
	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	public void draw() {
		// TODO Auto-generated method stub
		
	}

	public void draw(SpriteBatch batch) {
		// TODO Auto-generated method stub
		
	}

}
