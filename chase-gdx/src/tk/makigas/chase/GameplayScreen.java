package tk.makigas.chase;

import java.util.ArrayList;
import java.util.List;

import org.omg.PortableServer.POAManagerPackage.State;

import tk.makigas.chase.actor.*;
import tk.makigas.chase.listeners.InputAndroidMoveListener;
import tk.makigas.chase.listeners.InputAndroidShootListener;
import tk.makigas.chase.listeners.InputDesktopListener;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class GameplayScreen extends AbstractScreen {

	/** Escenario usado por el juego. */
	private Stage stage;
	
	/** Nave usada por el jugador. */
	private NaveActor nave;
	
	/** Escudo de la Tierra. */
	private EscudoActor escudo;
	
	/** HUDs usados para mostrar la vida de escudo y de nave. */
	private BarraActor vidaNave, vidaEscudo;
	
	/** Pads usados para controlar el juego en Android. */
	private PadActor padArriba, padAbajo, padShoot;
	
	/** Contador de tiempo usado para sincronizar algunos eventos. */
	private float timer,timer_meteorito,timer_repair;
	
	private float tiempo_llave,tiempo_alien,tiempo_meteorito ;
	
	/** Puntuación */
	private PuntuacionActor puntuacion;
	
	private List<AlienActor> aliens;
	
	private List<MeteoritoActor> meteoritos;
	
	private List<RepairActor> repairs;
	
	private List<BulletActor> bullets;
	
	private OrthographicCamera camara;
	
	public GameplayScreen(AlienChase game) {
		super(game);
	}
	
	@Override
	public void show() {
		aliens = new ArrayList<AlienActor>();
		bullets = new ArrayList<BulletActor>();
		meteoritos = new ArrayList<MeteoritoActor>();
		repairs = new ArrayList<RepairActor>();
		
		// Creamos un nuevo escenario y lo asociamos a la entrada.
		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();
		stage = new Stage(width, height, true, game.SB);
		Gdx.input.setInputProcessor(stage);
		
		// Crear fondo.
		Image imgFondo = new Image(AlienChase.MANAGER.get("fondo.png", Texture.class));
		AlienChase.MANAGER.get("musicaprincipal.ogg", Sound.class).stop();
		AlienChase.MANAGER.get("musicajuego.ogg", Sound.class).play();
		imgFondo.setFillParent(true);
		stage.addActor(imgFondo);
		
		// Creamos una nave.
		nave = new NaveActor();
		nave.setPosition(10, 10);
		stage.addActor(nave);

		// Creamos un escudo.
		escudo = new EscudoActor();
		escudo.setBounds(-5, 0, 5, stage.getHeight());
		stage.addActor(escudo);
		
		// Creamos los HUD de las naves.
		vidaNave = new BarraActor(nave);
		vidaEscudo = new BarraActor(escudo);		
		vidaNave.setPosition(stage.getWidth() - 150, stage.getHeight() - 20);
		//vidaEscudo.setPosition(stage.getWidth() - 150, stage.getHeight() - 28);
		stage.addActor(vidaNave);
		//stage.addActor(vidaEscudo);
		
		// Creamos los sistemas de entrada. En escritorio tendremos que usar
		// un listener que lo hace todo, mientras que para Android tendremos
		// que usar tres botones asociados cada uno a algo.
		if(Gdx.app.getType() == ApplicationType.Desktop) {
			stage.setKeyboardFocus(nave); // damos foco a nave.
			nave.addListener(new InputDesktopListener(nave, stage, bullets));
		} else if(Gdx.app.getType() == ApplicationType.Android) {
			// Creamos los pads.
			padArriba = new PadActor(0, 0);
			padAbajo = new PadActor(1, 0);
			padShoot = new PadActor(0, 1);
		
			// Los colocamos.
			padArriba.setPosition(10, 50);
			padAbajo.setPosition(10, 10);
			padShoot.setPosition(stage.getWidth() - 50, 10);
		
			// Añadimos los listeners.
			padArriba.addListener(new InputAndroidMoveListener(nave, 250f));
			padAbajo.addListener(new InputAndroidMoveListener(nave, -250f));
			padShoot.addListener(new InputAndroidShootListener(stage, nave, bullets));
		
			// Los añadimos al escenario.
			stage.addActor(padArriba);
			stage.addActor(padAbajo);
			stage.addActor(padShoot);
		}
		
		puntuacion = new PuntuacionActor(new BitmapFont());
		puntuacion.setPosition(10, stage.getHeight() - 10);
		puntuacion.puntuacion = 0;
		stage.addActor(puntuacion);
		
		// Finalmente inicializamos el contador de tiempo.
		timer = 2 + (float) Math.random();
		timer_meteorito = 15 + (float) Math.random();
		timer_repair = 20 + (float) Math.random();
		
		tiempo_llave = (float)20;
		tiempo_alien = (float)10;
		tiempo_meteorito = (float)0.50;
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL11.GL_COLOR_BUFFER_BIT);
		
		stage.act();
		timer -= delta;
		timer_meteorito -= delta;
		timer_repair -= delta;
		
		if(timer < 0)	
			dispararAlien();
		if (timer_meteorito < 0)
			generarMeteorito();
		if (timer_repair < 0)
			generarLlave();
		
		obtenerVida();
		comprobarColisionMeteorito();
		comprobarListas();
		comprobarColisiones();
		stage.draw();
	}
	
	private void obtenerVida(){
		RepairActor repair;
		for(int i = 0; i < repairs.size(); i++) {
			repair = repairs.get(i);
			if(repair.bb.overlaps(nave.bb)) {
				// Colisión alien-nave.
				repairs.get(i).remove();
				repairs.remove(i);
				nave.sumHealth(+0.5f);	
				AlienChase.MANAGER.get("heal.wav", Sound.class).play();
			}
		}
	}
	
	private void generarLlave(){
		RepairActor repair = new RepairActor();
		
		repair.setPosition(stage.getWidth(), 0.1f * stage.getHeight() + 
				0.8f * stage.getHeight() * (float) Math.random());
		repair.bb.x = repair.getX();
		repair.bb.y = repair.getY();
		stage.addActor(repair);
		repairs.add(repair);
		if(puntuacion.puntuacion > 20){
			tiempo_llave = (float) 12;
			timer_repair = tiempo_llave + (float) Math.random();
		}else{
			tiempo_llave = (float) 20;
			timer_repair = tiempo_llave + (float) Math.random();
		}
		
	}
	
	private void generarMeteorito() {
		MeteoritoActor meteorito = new MeteoritoActor();
		
		meteorito.setPosition(stage.getWidth(), 0.1f * stage.getHeight() + 
				0.8f * stage.getHeight() * (float) Math.random());
				
		meteorito.bb.x = meteorito.getX();
		meteorito.bb.y = meteorito.getY();
		stage.addActor(meteorito);
		meteoritos.add(meteorito);
		if(puntuacion.puntuacion > 20){
			tiempo_meteorito = (float) 5;
			timer_meteorito = tiempo_meteorito + (float) Math.random();
		}else{
			tiempo_meteorito = (float) 10;
			timer_meteorito = tiempo_meteorito + (float) Math.random();
		}
		
	}
	
	private void comprobarColisionMeteorito(){
		MeteoritoActor meteorito;
		for(int i = 0; i < meteoritos.size(); i++) {
			meteorito = meteoritos.get(i);
			if(meteorito.bb.overlaps(nave.bb)) {
				// Colisión alien-nave.
				meteoritos.get(i).remove();
				meteoritos.remove(i);
				nave.sumHealth(-0.5f);
				AlienChase.MANAGER.get("hit.ogg", Sound.class).play();
				if(nave.getHealth() <= 0) {
					GameOverScreen.setPuntuacionMaxima(puntuacion.getPuntuacionMaxima());
					game.setScreen(game.GAMEOVER);
				
				}
			} else {
				for(int j = 0; j < bullets.size(); j++) {
					if(bullets.get(j).bb.overlaps(meteorito.bb)) {
						// Colisión alien-bala.
						bullets.get(j).remove();
						bullets.remove(j);
						
					}
				}
			}
		}
	}

	private void comprobarListas() {
		for(int i = 0; i < aliens.size(); i++) {
			if(aliens.get(i).getRight() < 0) {
				aliens.get(i).remove();
				aliens.remove(i);
				if(escudo.getHealth() > 0.2f) {
					escudo.sumHealth(-0.2f);
					AlienChase.MANAGER.get("hit.ogg", Sound.class).play();
					puntuacion.puntuacion = puntuacion.puntuacion - 2;
					if(puntuacion.puntuacion < 0){
						GameOverScreen.setPuntuacionMaxima(puntuacion.getPuntuacionMaxima());
						game.setScreen(game.GAMEOVER);
					}
						
					
				} else {
					GameOverScreen.setPuntuacionMaxima(puntuacion.getPuntuacionMaxima());
					game.setScreen(game.GAMEOVER);
				}
			}
		}
		for(int i = 0; i < bullets.size(); i++) {
			if(bullets.get(i).getX() > stage.getWidth()) {
				bullets.get(i).remove();
				bullets.remove(i);
			}
		}
		/* Comprobar si llegan al final de la pantalla los meteoritos  */
		
		for(int i = 0; i < meteoritos.size(); i++) {
			if(meteoritos.get(i).getRight() < 0) {
				meteoritos.get(i).remove();
				meteoritos.remove(i);
			}
		}
		
	}
	
	private void comprobarColisiones() {
		AlienActor alien;
		for(int i = 0; i < aliens.size(); i++) {
			alien = aliens.get(i);
			if(alien.bb.overlaps(nave.bb)) {
				// Colisión alien-nave.
				aliens.get(i).remove();
				aliens.remove(i);
				nave.sumHealth(-0.3f);
				AlienChase.MANAGER.get("hit.ogg", Sound.class).play();
				if(nave.getHealth() <= 0) {
					GameOverScreen.setPuntuacionMaxima(puntuacion.getPuntuacionMaxima());
					game.setScreen(game.GAMEOVER);
				
				}
			} else {
				for(int j = 0; j < bullets.size(); j++) {
					if(bullets.get(j).bb.overlaps(alien.bb)) {
						// Colisión alien-bala.
						aliens.get(i).remove();
						aliens.remove(i);
						bullets.get(j).remove();
						bullets.remove(j);
						AlienChase.MANAGER.get("explosion.ogg", Sound.class).play();
						puntuacion.puntuacion++;
						if (puntuacion.getPuntuacion() > puntuacion.getPuntuacionMaxima()){
							puntuacion.setPuntuacionMaxima(puntuacion.puntuacion);
						}
					}
				}
			}
		}
	}
	private void dispararAlien() {
		AlienActor alien = new AlienActor();
		alien.setPosition(stage.getWidth(), 0.1f * stage.getHeight() + 
				0.8f * stage.getHeight() * (float) Math.random());
		alien.bb.x = alien.getX();
		alien.bb.y = alien.getY();
		stage.addActor(alien);
		aliens.add(alien);
		if(puntuacion.puntuacion < 5){
			tiempo_alien = (float) 1;
			timer = tiempo_alien + (float) Math.random();
		}else if (puntuacion.puntuacion < 20) {
			tiempo_alien = (float) 0.50;
			timer = tiempo_alien + (float) Math.random();
		}else{
			tiempo_alien = (float) 0.20;
			timer = tiempo_alien + (float) Math.random();
		}
		
	}

	@Override
	public void hide() {
		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

	@Override
	public void resize(int width, int height) {
		stage.setViewport(width, height, true);
		vidaNave.setPosition(stage.getWidth() - 150, stage.getHeight() - 20);
		//vidaEscudo.setPosition(stage.getWidth() - 150, stage.getHeight() - 28);
		if(Gdx.app.getType() == ApplicationType.Android && padShoot != null)
			padShoot.setPosition(stage.getWidth() - 50, 10);
	}
}
