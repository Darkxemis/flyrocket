package tk.makigas.chase.actor;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class PuntuacionActor extends Actor {
	
	public int puntuacion;
	public int puntuacion_maxima;
	
	private BitmapFont font;
	

	public PuntuacionActor(BitmapFont font) {
		this.font = font;
	}
	
	public void setPuntuacionMaxima(int puntuacion){
		puntuacion_maxima = puntuacion;
	}
	
	public int getPuntuacionMaxima(){
		return this.puntuacion_maxima;
	}
	
	public int getPuntuacion(){
		return this.puntuacion;
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		font.draw(batch, "PUNTOS: " + puntuacion, 20, 20);//getX() 
		font.draw(batch, "PUNTOS MAXIMO OPTENIDOS: " + puntuacion_maxima, 500, 20);//getX() 
	}
}
