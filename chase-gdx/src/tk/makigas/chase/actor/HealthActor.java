package tk.makigas.chase.actor;

public interface HealthActor {

	/**
	 * Obtiene la vida actual de la entidad.
	 * @return vida actual.
	 */
	public float getHealth();
	
	/**
	 * Cambia la vida actual.
	 * @param health nuevo valor de la vida.
	 */
	public void setHealth(float health);
	
	/**
	 * Suma (o resta) vida a la nave.
	 * @param sum cantidad de vida sumada o restada.
	 */
	public void sumHealth(float sum);
	
}
